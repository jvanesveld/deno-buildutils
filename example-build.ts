import { runCommands } from "https://gitlab.com/jvanesveld/deno-buildutils/-/raw/main/mod.ts";

const commands = [
  "deno run --allow-read --allow-write example-pack.ts",
  "deno compile --allow-all mod.js",
];

await runCommands(commands);
