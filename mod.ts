import {
  encode,
  decode,
} from "https://deno.land/std@0.118.0/encoding/base64.ts";

export type Resources = {
  [key: string]: string | { file: string; type: "base64" | "text" };
};

/**
 * Pack resource files into a js/ts file
 * @param resources Resources to pack, key will be the name under which the file is exported
 * @param output The js/ts file that the resources are saved to
 */
export async function packResources(resources: Resources, output: string) {
  let result = `import {decode} from "https://deno.land/std@0.118.0/encoding/base64.ts";\n`;
  for (const [key, resource] of Object.entries(resources)) {
    let content: string;
    if (typeof resource == 'string' || resource.type == 'text') {
      content = await Deno.readTextFile(typeof resource === "string" ? resource : resource.file);
      result += `export const ${key} = ${JSON.stringify(content)};\n`;
    } else {
      content = encode(await Deno.readFile(resource.file));
      result += `export const ${key} = decode(${JSON.stringify(content)});\n`;
    }
  }

  await Deno.writeTextFile(output, result);
}


/**
 * Runs a command
 * @param cmd String command, same as you would type in the cli
 * @param exitOnFailure Wether to exit if sub command failed
 */
export async function runCmd(cmd: string, exitOnFailure = true) {
  const result = await Deno.run({
    cmd: cmd.split(" ").map(part => part.trim()).filter(part => part !== ""),
  }).status();

  if (!result.success && exitOnFailure) {
    Deno.exit(1);
  }
}

/**
 * Runs commands one after another (by default exits when command fails)
 * Perfect for packing and compiling, like running:
 * deno run --allow-run --allow-write my-packing-script.ts
 * deno compile my-script-with-resource.ts
 * @param commands The commands to run
 */
export async function runCommands(
  commands: (string | { cmd: string; exitOnFailure: boolean })[]
) {
  for (const cmd of commands) {
    if (typeof cmd === "string") {
      await runCmd(cmd);
    } else {
      await runCmd(cmd.cmd, cmd.exitOnFailure);
    }
  }
}
