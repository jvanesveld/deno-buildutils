import { packResources } from "https://gitlab.com/jvanesveld/deno-buildutils/-/raw/main/mod.ts";

const files = {
  html: "./index.html",
  js: "./index.js",
};

await packResources(files, "./resource/mod.ts");
